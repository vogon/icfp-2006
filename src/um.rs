use std::collections::BTreeSet;
use std::io::{Read, Write};
use std::num::Wrapping;

extern crate time;
use self::time::precise_time_ns;

pub type Platter = u32;

#[derive(Debug)]
pub enum UMStatus {
	// the program is still running
	Continue,
	// the program requested to halt
	Halt,	
}

#[derive(Debug)]
pub enum UMFailure {
	// the execution finger points to a platter that does not encode a valid
	// instruction
	InvalidInstruction,
	// the program indexed or amended an inactive array
	InactiveArrayAccess,
	// the program tried to index or amend an array out of range
	OutOfBoundArrayAccess,
	// the program abandoned the program array
	AbandonedProgramArray,
	// the program abandoned an inactive array
	AbandonedArrayNotActive,
	// the program loaded a program from an inactive array
	InactiveArrayProgramLoaded,
	// the program output an out-of-range character
	InvalidOutputCharacter,
	// the execution finger points out of range in array 0
	InvalidEF,
}

pub struct PerformanceCounter {
	pub n_insns: u64,
	pub ns: u64
}

pub struct UM {
	regs: [Platter; 8],
	ef: Platter,
	array_0: Vec<Platter>,
	arrays: Vec<Vec<Platter>>,
	free_array_ids: BTreeSet<Platter>,
	n_insns: [u64; 16],
	ns: [u64; 16],
}

impl UM {
	pub fn new(program: &[Platter]) -> UM {
		let mut new_um = UM {
			regs: [0; 8],
			ef: 0,
			arrays: vec![Vec::new()],
			array_0: Vec::with_capacity(program.len()),
			free_array_ids: BTreeSet::new(),
			n_insns: [0; 16],
			ns: [0; 16],
		};

		new_um.array_0.resize(program.len(), 0);

		for i in 0..(program.len()) {
			new_um.array_0[i] = program[i];
		}

		return new_um;
	}

	pub fn step(&mut self, input: &mut Read, output: &mut Write) 
			-> Result<UMStatus, UMFailure> {
		// fetch insn
		let ns_start = time::precise_time_ns();

		let insn = match self.array_0.get(self.ef as usize) {
			Some(i) => *i,
			// EF off the end of the program array
			None => return Err(UMFailure::InvalidEF)
		};

		// break out the individual fields
		let opcode = (insn >> 28) & 0xF;
		let imm_a = ((insn >> 25) & 0x7) as usize;
		let a = ((insn >> 6) & 0x7) as usize;
		let b = ((insn >> 3) & 0x7) as usize;
		let c = (insn & 0x7) as usize;
		let immediate = insn & 0x01FFFFFF;

		match opcode {
			0 => {
				// conditional move
				if self.regs[c] != 0 {
					self.regs[a] = self.regs[b];
				}

				self.ef += 1;
			},
			1 => {
				// array index
				let val = self.index_array(self.regs[b], self.regs[c]);

				match val {
					Ok(_) => { self.regs[a] = val.unwrap(); self.ef += 1; },
					Err(e) => { return Err(e); }
				}
			},
			2 => {
				// array amendment
				let id = self.regs[a];
				let offset = self.regs[b];
				let value = self.regs[c];

				match self.amend_array(id, offset, value) {
					Ok(_) => { self.ef += 1; },
					Err(e) => { return Err(e); }
				}
			},
			3 => {
				// addition
				self.regs[a] = 
					(Wrapping(self.regs[b]) + Wrapping(self.regs[c])).0;

				self.ef += 1;
			},
			4 => {
				// multiplication
				self.regs[a] =
					(Wrapping(self.regs[b]) * Wrapping(self.regs[c])).0;

				self.ef += 1;
			},
			5 => {
				// division
				self.regs[a] =
					(Wrapping(self.regs[b]) / Wrapping(self.regs[c])).0;

				self.ef += 1;
			},
			6 => {
				// "not-and" (NAND)
				self.regs[a] = !(self.regs[b] & self.regs[c]);

				self.ef += 1;
			},
			7 => {
				// halt
				return Ok(UMStatus::Halt);
			},
			8 => {
				// array allocation
				let capacity = self.regs[c];
				let new_id = self.allocate_array(capacity);

				self.regs[b] = new_id;
				self.ef += 1;
			},
			9 => {
				// array abandonment
				let id = self.regs[c];

				match self.abandon_array(id) {
					Ok(_) => { self.ef += 1; },
					Err(e) => { return Err(e) }
				};
			},
			10 => {
				// output
				let c_val = self.regs[c];

				if c_val > 255 {
					return Err(UMFailure::InvalidOutputCharacter);
				} else {
					output.write(&[c_val as u8]);
					self.ef += 1;
				}
			},
			11 => {
				// input
				output.flush();

				match input.bytes().next() {
					Some(res) => {
						match res {
							Ok(13) => { /* drop CR on the floor */ },
							Ok(ch) => {
								self.regs[c] = (ch as u32) & 0xFF;
								self.ef += 1;
							},
							Err(e) => panic!(e)
						}
					},
					None => {
						self.regs[c] = 0xFFFFFFFF; self.ef += 1; 
					}
				}
			},
			12 => {
				// load program
				match self.load_program(b, c) {
					Ok(_) => () /* EF is reset by load_program() */,
					Err(e) => return Err(e)
				};
			},
			13 => {
				// "orthography": load immediate
				self.regs[imm_a] = immediate;
				self.ef += 1;
			},
			_ => {
				// insns 14-15 are undefined
				return Err(UMFailure::InvalidInstruction);
			}
		};

		let ns_end = time::precise_time_ns();

		self.n_insns[opcode as usize] += 1;
		self.ns[opcode as usize] += ns_end - ns_start;

		// if we made it out here, increment EF
		return Ok(UMStatus::Continue);
	}

	pub fn get_ef(&self) -> Platter {
		return self.ef;
	}

	pub fn dump_regs(&self) -> [Platter; 8] {
		return self.regs.clone();
	}

	pub fn get_perf_counter(&self, opcode: usize) -> PerformanceCounter {
		return PerformanceCounter { 
			n_insns: self.n_insns[opcode], ns: self.ns[opcode]
		};
	}

	pub fn peek_array(&self, id: Platter, offset: Platter) -> Option<Platter> {
		return match self.index_array(id, offset) {
			Ok(x) => Some(x),
			Err(_) => None
		}
	}

	fn allocate_array(&mut self, capacity: Platter) -> Platter {
		//println!("allocating new array");

		// choose an ID for the new array
		let new_id = match self.free_array_ids.iter().next() {
			Some(id) => {
				*id as usize
			},
			None => {
				let old_len = self.arrays.len();
				self.arrays.push(Vec::new());

				old_len
			}
		};

		self.arrays[new_id].resize(capacity as usize, 0);
		self.free_array_ids.remove(&(new_id as Platter));

		return new_id as Platter;
	}

	fn index_array(&self, id: Platter, offset: Platter) 
			-> Result<Platter, UMFailure> {
		return match id {
			0 => {
				match self.array_0.get(offset as usize) {
					Some(i) => Ok(*i),
					None => Err(UMFailure::OutOfBoundArrayAccess)
				}
			},
			_ => {
				match self.arrays.get(id as usize) {
					Some(arr) => match arr.get(offset as usize) {
						Some(i) => Ok(*i),
						None => Err(UMFailure::OutOfBoundArrayAccess)
					},
					None => Err(UMFailure::InactiveArrayAccess)
				}
			}
		};
	}

	fn amend_array(&mut self, id: Platter, offset: Platter, value: Platter) 
			-> Result<(), UMFailure> {
		return match id {
			0 => {
				if offset as usize > self.array_0.len() {
					Err(UMFailure::OutOfBoundArrayAccess)
				} else {
					self.array_0[offset as usize] = value;
					Ok(())
				}
			},
			_ => {
				match self.arrays.get_mut(id as usize) {
					Some(arr) => 
						if offset as usize > arr.len() {
							Err(UMFailure::OutOfBoundArrayAccess)
						} else {
							arr[offset as usize] = value;
							Ok(())
						},
					None => Err(UMFailure::InactiveArrayAccess)
				}
			}
		}
	}

	fn abandon_array(&mut self, id: Platter) -> Result<(), UMFailure> {
		// println!("abandoning array");

		if id == 0 {
			// abandoning the program array
			return Err(UMFailure::AbandonedProgramArray);
		}

		if self.free_array_ids.contains(&id) || 
				id as usize >= self.arrays.len() {
			// abandoning an array not present in the list of arrays
			return Err(UMFailure::AbandonedArrayNotActive);
		} else {
			self.free_array_ids.insert(id);
			self.arrays[id as usize].clear();

			return Ok(());
		}
	}

	fn load_program(&mut self, b: usize, c: usize) -> Result<(), UMFailure> {
		// println!("")

		// fetch args from regs
		let array_id = self.regs[b];
		let new_ef = self.regs[c];

		// println!("loading program from array {} @ {}", array_id, new_ef);

		match array_id {
			0 => {
				// fast path: don't duplicate 0-array
			},
			_ => {
				// slow path: copy non-0 array to 0-array

				// load B array and copy it
				let old_program = match self.arrays.get(array_id as usize) {
						Some(a) => a,
						// trying to load program out of an unallocated array
						None => return Err(UMFailure::InactiveArrayProgramLoaded)
				};

				self.array_0.clone_from(old_program);
			}
		};

		// reset the EF from C
		self.ef = new_ef;

		return Ok(());
	}
}