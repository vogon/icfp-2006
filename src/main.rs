use std::env;
use std::error::Error;
use std::fs::File;
use std::io;

extern crate byteorder;
use byteorder::{BigEndian, ReadBytesExt};

extern crate getopts;
use getopts::Options;

mod um;
use um::{UM, UMStatus};

fn dump_state(um: &UM) {
	match um.peek_array(0, um.get_ef()) {
		Some(insn) => println!("EF = 0x{:08x} (0x{:08x})", um.get_ef(), insn),
		None => println!("EF = 0x{:08x} (???)", um.get_ef())
	};

	let regs = um.dump_regs();

	println!("r0 = 0x{:08x} r1 = 0x{:08x} r2 = 0x{:08x} r3 = 0x{:08x}",
		regs[0], regs[1], regs[2], regs[3]);
	println!("r4 = 0x{:08x} r5 = 0x{:08x} r6 = 0x{:08x} r7 = 0x{:08x}",
		regs[4], regs[5], regs[6], regs[7]);
}

fn print_usage(program: &str, opts: Options) {
	let brief = format!("Usage: {} program [options]", program);
	print!("{}", opts.usage(&brief));
}

fn main() {
	let args: Vec<_> = env::args().collect();

	let mut opts = Options::new();
	opts.optflag("", "dump", "dump state before every step");
	opts.optflag("", "perf", "dump perf counters on halt");
	opts.optflag("v", "verbose", "be noisier");
	opts.optopt("l", "limit", "execute for N instructions", "N");

	if args.len() == 1 {
		print_usage(&args[0], opts);
		return;
	}

	let matches = match opts.parse(&args[1..]) {
		Ok(m) => m,
		Err(f) => { panic!(f.to_string()) }
	};

	// grab limit parameter
	let limit = match matches.opt_str("limit") {
		// parameter present
		Some(arg) => match arg.parse::<usize>() {
			// argument was an integer
			Ok(n) => Some(n),
			// argument wasn't an integer
			Err(_) => {
				println!("error: non-integral argument passed to -l/--limit");
				print_usage(&args[0], opts);
				return;
			}
		},
		// parameter not present
		None => None
	};

	// grab dump parameter
	let dump = matches.opt_present("dump");

	let mut file = match File::open(&args[1]) {
		Err(why) => panic!("couldn't open {}: {}", 
			args[1], Error::description(&why)),
		Ok(file) => file
	};

	let mut program = Vec::new();

	loop {
		let n = match file.read_u32::<BigEndian>() {
			Ok(n) => n,
			// stop reading by running off the end of the file
			Err(byteorder::Error::UnexpectedEOF) => break,
			Err(why) => panic!("read_u32 returned {}", why)
		};

		program.push(n);
	}

	if matches.opt_present("verbose") {
		println!("{} loaded and running.", args[1]);
	}

	let mut machine = UM::new(&program);
	let mut insns_run = 0;

	let stdin = io::stdin();
	let stdout = io::stdout();

	let in_lock = &mut stdin.lock();
	let out_lock = &mut stdout.lock();

	loop {
		if dump {
			dump_state(&machine);
			println!("==================================================");
		}

		match machine.step(in_lock, out_lock) {
			Ok(UMStatus::Continue) => { 
				insns_run += 1;

				match limit {
					Some(n) => if insns_run >= n { break; },
					None => ()
				};
			},
			Ok(UMStatus::Halt) => {
				if matches.opt_present("verbose") {
					println!("");
					println!("machine halted normally.");
				}
				break;
			},
			Err(e) => {
				println!("");
				println!("machine halted with failure: {:?}", e);
				dump_state(&machine);
				break;
			}
		}
	}

	if matches.opt_present("perf") {
		for opcode in 0..14 {
			let pc = machine.get_perf_counter(opcode);

			println!("opcode {}: {} executed, {} ns ({:.1} ns/insn)", opcode, 
				pc.n_insns, pc.ns, pc.ns as f64 / pc.n_insns as f64);
		}
	}
}